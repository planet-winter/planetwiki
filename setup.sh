#!/bin/bash

echo "Setting up your wiki" 
echo 
echo "installing components"
sleep 1

echo "trying to install virtualenv"
if [[ $EUID -ne 0 ]]; then
    cmd="sudo yum"
else
    cmd="yum"
fi
if [[ $(which yum) ]]; then
  $cmd install -y python-virtualenv || exit 1 >/dev/null 2>&1
  $cmd install -y python-sphinx || exit 1 >/dev/null 2>&1
else
  echo "Please make sure python virtualenv and python sphinx are installed"
  echo "Trying to continue assuming these dependencies are met."
fi

echo ""
echo "creating virtualenv" 
HOMEBASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$HOMEBASE"
mkdir -p lib/virtualenv
virtualenv --no-site-packages lib/virtualenv
source lib/virtualenv/bin/activate
pip install -r lib/requirements.txt

echo ""
echo "installing local hooks"
cp -f lib/hooks/pre-commit .git/hooks/
cp -f lib/hooks/post-commit .git/hooks/

echo ""
echo "trigger first build"
.git/hooks/post-commit

exit 0
