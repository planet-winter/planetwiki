planetwiki
==========

a locally built wiki similar to [readthedocs.org](http://readthedocs.org/). 

builds a html wiki on git commit from _reStructuredText_ files using _sphinx_ and the _readthedocs_ theme.

usage
-----

   * edit the wiki files in the _src_ folder using [reStructuredText syntax](http://www.sphinx-doc.org/en/stable/rest.html)
   * commit the changes to the git repository, [sphinx](http://www.sphinx-doc.org/) builds a static html wiki in the _html_ folder


setup
-----

execute ``./setup.sh``
this attempts to install required packages with _yum_ and setup sphinx within a python virtual-environment


author
------

daniel winter
